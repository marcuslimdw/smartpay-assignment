# To run

`sbt run` to start the server and `sbt test` to run tests.

# Assumptions

* If the upstream server fails, regardless of reason, the client should try again rather than expecting the server to do so. In theory, logic to intelligently do this (exponential backoff for 500s, but no retries for 429 etc.) could be implemented.
* The upstream server "rarely fails" and calls are "rare". One could use, for example, a cache, if the latter was not true and getting reasonably up-to-date data was not crucial.

# Design decisions

* There are no end-to-end integration tests (or contract tests) because this is a proof of concept. A production app should have them.
* App construction and launch are separate responsibilities and therefore each is now performed by a different component.
* The effect type has been extracted so that it can be more easily changed in the future.
* Ideally, the token would not be stored in the repository (huge security risk). Use a third party secret manager. 
