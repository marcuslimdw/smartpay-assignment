package prices.routes.protocol

import io.circe.Json
import munit.FunSuite
import prices.services.PriceInfo

class PriceResponseTest extends FunSuite {

  test("should successfully serialize a PriceResponse to JSON") {
    val priceInfo = PriceInfo("sc2-micro", BigDecimal("1.234"), "")
    val priceResponse = PriceResponse(priceInfo)
    val expected = Json.obj(
      ("kind", Json.fromString("sc2-micro")),
      ("amount", Json.fromBigDecimal(BigDecimal("1.234")))
    )
    val actual = PriceResponse.encoder(priceResponse)
    assertEquals(actual, expected)
  }
}
