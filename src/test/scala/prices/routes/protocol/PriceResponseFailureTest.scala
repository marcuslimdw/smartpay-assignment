package prices.routes.protocol

import io.circe.Json
import munit.FunSuite

class PriceResponseFailureTest extends FunSuite {

  class PriceResponseTest extends FunSuite {

    test("should successfully serialize a PriceResponseFailure to JSON") {
      val priceResponseFailure = PriceResponseFailure("error message")
      val expected = Json.obj(
        ("error", Json.fromString("error message"))
      )
      val actual = PriceResponseFailure.encoder(priceResponseFailure)
      assertEquals(actual, expected)
    }
  }
}
