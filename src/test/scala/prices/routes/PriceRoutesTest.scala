package prices.routes

import cats.effect.IO
import cats.effect.unsafe.implicits.global
import munit.FunSuite
import org.http4s.Method.GET
import org.http4s.Request
import org.http4s.Status.{BadRequest, InternalServerError, Ok, TooManyRequests}
import org.http4s.implicits.http4sLiteralsSyntax
import prices.data.{InstanceKind, SmartcloudServerError}
import prices.services.{PriceInfo, PriceService}

class PriceRoutesTest extends FunSuite {

  private def stubService(returnValue: Either[Throwable, PriceInfo]): PriceService[IO] = (_: InstanceKind) => returnValue
    .fold(IO.raiseError, IO.pure)

  test("GET /prices should return HTTP 200 OK with a PriceInfo if the upstream server returns a valid response") {
    val service = stubService(Right(PriceInfo("sc2-micro", BigDecimal("1.234"), "")))
    val routes = PriceRoutes(service).routes
    val request = Request[IO](GET, uri"prices?kind=sc2-micro")
    val response = routes(request).value.unsafeRunSync().get
    assertEquals(response.status, Ok)
  }

  test("GET /prices should return HTTP 404 Not Found if no instance kind is provided") {
    val service = stubService(Right(PriceInfo("sc2-micro", BigDecimal("1.234"), "")))
    val routes = PriceRoutes(service).routes
    val request = Request[IO](GET, uri"prices")
    assert(routes(request).value.unsafeRunSync().isEmpty)
  }

  test("GET /prices should pass through a HTTP 429 Too Many Requests status code from the upstream server") {
    val service = stubService(Left(SmartcloudServerError(TooManyRequests, "")))
    val routes = PriceRoutes(service).routes
    val request = Request[IO](GET, uri"prices?kind=sc2-micro")
    val response = routes(request).value.unsafeRunSync().get
    assertEquals(response.status, TooManyRequests)
  }

  test("GET /prices should return HTTP 500 Internal Server Error if the request to the upstream server fails") {
    val service = stubService(Left(SmartcloudServerError(BadRequest, "")))
    val routes = PriceRoutes(service).routes
    val request = Request[IO](GET, uri"prices?kind=sc2-micro")
    val response = routes(request).value.unsafeRunSync().get
    assertEquals(response.status, InternalServerError)
  }
}
