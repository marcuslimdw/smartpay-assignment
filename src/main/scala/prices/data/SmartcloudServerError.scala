package prices.data

import org.http4s.Status

case class SmartcloudServerError(status: Status, body: String) extends Throwable
