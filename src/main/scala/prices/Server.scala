package prices

import cats.effect._
import com.comcast.ip4s._
import fs2.Stream
import org.http4s.ember.server.EmberServerBuilder
import org.http4s.server.middleware.Logger
import prices.config.AppConfig

object Server {

  def serve[F[_]: Async](config: AppConfig): Stream[F, ExitCode] = {
    val httpApp      = SmartcloudHttpApp.make(config.smartcloud)
    val serverConfig = config.server
    val server = EmberServerBuilder.default
      .withHost(Host.fromString(serverConfig.host).get)
      .withPort(Port.fromInt(serverConfig.port).get)
      .withHttpApp(Logger.httpApp(logHeaders = true, logBody = true)(httpApp))
      .build
      .useForever
    Stream.eval(server)
  }
}
