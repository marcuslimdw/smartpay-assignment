package prices.services

import cats.effect.kernel.{Concurrent, Resource}
import cats.syntax.functor._
import io.circe.generic.codec.DerivedAsObjectCodec.deriveCodec
import org.http4s.Method._
import org.http4s.circe.jsonOf
import org.http4s.client.Client
import org.http4s.headers.{Accept, Authorization}
import org.http4s.{AuthScheme, Credentials, EntityDecoder, MediaType, Request, Response, Uri}
import prices.data.{InstanceKind, SmartcloudServerError}
import prices.services.SmartcloudPriceService.PriceConfig

object SmartcloudPriceService {

  final case class PriceConfig(
      baseUri: Uri,
      token: String
  )

  def make[F[_]: Concurrent](config: PriceConfig, clientResource: Resource[F, Client[F]]): PriceService[F] = new SmartcloudPriceService(config, clientResource)
}

private final class SmartcloudPriceService[F[_]: Concurrent](config: PriceConfig, client: Resource[F, Client[F]]) extends PriceService[F] {

  implicit val priceInfoDecoder: EntityDecoder[F, PriceInfo] = jsonOf[F, PriceInfo]

  override def getForKind(instanceKind: InstanceKind): F[PriceInfo] = client.use {
    val request = Request[F](GET, config.baseUri / "instances" / instanceKind.getString).putHeaders(
      Authorization(Credentials.Token(AuthScheme.Bearer, config.token)),
      Accept(MediaType.application.json)
    )
    _.expectOr(request)(formatResponse)
  }

  private def formatResponse(response: Response[F]): F[Throwable] =
    response.body.through(fs2.text.utf8.decode).compile.string.map(SmartcloudServerError(response.status, _))
}
