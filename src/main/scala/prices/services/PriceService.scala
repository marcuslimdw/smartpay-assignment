package prices.services

import prices.data.InstanceKind

trait PriceService[F[_]] {

  def getForKind(instanceKind: InstanceKind): F[PriceInfo]
}

case class PriceInfo(kind: String, price: BigDecimal, timestamp: String)
