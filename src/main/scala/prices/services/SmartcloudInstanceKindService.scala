package prices.services

import cats.effect._
import cats.implicits._
import org.http4s._
import org.http4s.circe._
import prices.data._

object SmartcloudInstanceKindService {

  final case class InstanceKindConfig(
      baseUri: String,
      token: String
  )

  def make[F[_]: Concurrent](config: InstanceKindConfig): InstanceKindService[F] = new SmartcloudInstanceKindService(config)

  private final class SmartcloudInstanceKindService[F[_]: Concurrent](
      config: InstanceKindConfig
  ) extends InstanceKindService[F] {

    implicit val instanceKindsEntityDecoder: EntityDecoder[F, List[String]] = jsonOf[F, List[String]]

    val getAllUri = s"${config.baseUri}/instances"

    override def getAll: F[List[InstanceKind]] =
      List("sc2-micro", "sc2-small", "sc2-medium") // Dummy data. Your implementation should call the smartcloud API.
        .map(InstanceKind)
        .pure[F]
  }
}
