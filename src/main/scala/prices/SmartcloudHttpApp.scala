package prices

import cats.effect.kernel.Async
import cats.syntax.semigroupk._
import org.http4s.ember.client.EmberClientBuilder
import org.http4s.{HttpApp, Uri}
import prices.config.AppConfig.SmartcloudConfig
import prices.routes.{InstanceKindRoutes, PriceRoutes}
import prices.services.SmartcloudInstanceKindService.InstanceKindConfig
import prices.services.SmartcloudPriceService.PriceConfig
import prices.services.{SmartcloudInstanceKindService, SmartcloudPriceService}

object SmartcloudHttpApp {

  def make[F[_]: Async](config: SmartcloudConfig): HttpApp[F] = {
    val instanceKindService = SmartcloudInstanceKindService
      .make(InstanceKindConfig(config.baseUri, config.token))

    val priceService = SmartcloudPriceService
      .make(PriceConfig(Uri.unsafeFromString(config.baseUri), config.token), EmberClientBuilder.default.build)

    val allRoutes = InstanceKindRoutes(instanceKindService).routes <+> PriceRoutes(priceService).routes
    allRoutes.orNotFound
  }
}
