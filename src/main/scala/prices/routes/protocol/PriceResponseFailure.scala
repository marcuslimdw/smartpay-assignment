package prices.routes.protocol

import io.circe.syntax.EncoderOps
import io.circe.{Encoder, Json}

final case class PriceResponseFailure(errorMessage: String)

object PriceResponseFailure {

  implicit val encoder: Encoder[PriceResponseFailure] = Encoder.instance[PriceResponseFailure] {
    case PriceResponseFailure(error) => Json.obj(
      "error" -> error.asJson
    )
  }
}
