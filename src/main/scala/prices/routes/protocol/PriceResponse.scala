package prices.routes.protocol

import io.circe.syntax.EncoderOps
import io.circe.{Encoder, Json}
import prices.services.PriceInfo

final case class PriceResponse(priceInfo: PriceInfo)

object PriceResponse {

  implicit val encoder: Encoder[PriceResponse] = Encoder.instance[PriceResponse] {
    case PriceResponse(priceInfo) => Json.obj(
      "kind" -> priceInfo.kind.asJson,
      "amount" -> priceInfo.price.asJson
    )
  }
}
