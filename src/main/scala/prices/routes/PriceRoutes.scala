package prices.routes

import cats.effect._
import cats.implicits._
import org.http4s.circe.CirceEntityCodec.circeEntityEncoder
import org.http4s.circe._
import org.http4s.dsl.Http4sDsl
import org.http4s.server.Router
import org.http4s.{EntityEncoder, HttpRoutes}
import prices.data.{InstanceKind, SmartcloudServerError}
import prices.routes.protocol.PriceResponseFailure.encoder
import prices.routes.protocol._
import prices.services.PriceService

final case class PriceRoutes[F[_] : Sync](priceService: PriceService[F]) extends Http4sDsl[F] {

  val prefix = "/prices"

  implicit val priceResponseEncoder: EntityEncoder[F, List[PriceResponse]] = jsonEncoderOf[F, List[PriceResponse]]

  private val get: HttpRoutes[F] = HttpRoutes.of {
    case GET -> Root :? InstanceKindParamMatcher(instanceKind) => priceService
      .getForKind(instanceKind)
      .flatMap(priceInfo => Ok(PriceResponse(priceInfo)))
      .recoverWith {
        case SmartcloudServerError(TooManyRequests, _) => TooManyRequests(PriceResponseFailure("Upstream server received too many requests."))
        case SmartcloudServerError(InternalServerError, body) => InternalServerError(PriceResponseFailure(s"Upstream server failed with error $body."))
        case _ => InternalServerError(PriceResponseFailure("An unknown error occurred."))
      }
  }

  def routes: HttpRoutes[F] = Router(
    prefix -> get
  )

  object InstanceKindParamMatcher extends QueryParamDecoderMatcher[InstanceKind]("kind")
}
