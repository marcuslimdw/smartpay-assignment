package prices.config

import cats.effect.kernel.Sync
import pureconfig.ConfigSource
import pureconfig.generic.auto._

case class AppConfig(
  server: AppConfig.ServerConfig,
  smartcloud: AppConfig.SmartcloudConfig
)

object AppConfig {

  case class ServerConfig(
    host: String,
    port: Int
  )

  case class SmartcloudConfig(
    baseUri: String,
    token: String
  )

  def load[F[_] : Sync]: F[AppConfig] =
    Sync[F].delay(ConfigSource.default.loadOrThrow[AppConfig])
}
