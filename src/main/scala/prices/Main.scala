package prices

import cats.effect.{ IO, IOApp }

import prices.config.AppConfig

object Main extends IOApp.Simple {

  def run: IO[Unit] = AppConfig.load[IO].flatMap(Server.serve[IO](_).compile.drain)
}
